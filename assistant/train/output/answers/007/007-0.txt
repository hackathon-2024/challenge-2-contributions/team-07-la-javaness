```bash
curl -L -X POST 'https://open-api.tiktok.com/video/query/' \
-H 'Content-Type: application/json' \
--data-raw '{
    "access_token": "act.example12345Example12345Example",
    "filters": {
        "video_ids": ["1234568030997662933"]
    },
    "fields": ["like_count"]
}'
```
Explanation about parameters can be found here: https://developers.tiktok.com/doc/research-api-specs-query-videos/