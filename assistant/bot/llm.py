# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os

from loguru import logger
from llama_index.llms import HuggingFaceLLM
from llama_index.embeddings import HuggingFaceEmbedding
import torch
from transformers import (
    BitsAndBytesConfig,
    AutoModelForCausalLM,
    AutoTokenizer,
    AutoModel,
)
from peft import AutoPeftModelForCausalLM


def get_fine_tuned_llm(model_path: str = ""):
    logger.info(f"Trying to log model at path {model_path}")
    # BitsAndBytesConfig int-4 config
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.float16,
    )

    # Load Model with PEFT adapter
    model = AutoPeftModelForCausalLM.from_pretrained(
        model_path, device_map="cuda:0", torch_dtype=torch.float16
    )
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    logger.info("Model and Tokenizer successfully loaded")
    logger.info("Creating Llama-index LLM instance...")
    return HuggingFaceLLM(
        model=model,
        is_chat_model=True,
        tokenizer=tokenizer,
    )


def get_llm(model_path: str):
    model = AutoModelForCausalLM.from_pretrained(
        model_path, device_map="cuda:0", torch_dtype=torch.float16
    )
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    return HuggingFaceLLM(
        model=model,
        is_chat_model=True,
        tokenizer=tokenizer,
    )


def get_embedding_model(model_path: str):
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModel.from_pretrained(
        model_path, device_map="cuda:1", torch_dtype=torch.float16
    )
    return HuggingFaceEmbedding(model=model, tokenizer=tokenizer)
