# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from collections.abc import Callable
import os

from loguru import logger
from tqdm import tqdm
import pandas as pd

from assistant.bot.utils import (
    ask_llm_is_code,
    interrogate_rag,
    generate_pre_answer,
    self_evaluation_loop,
    self_consistency,
    save_output,
)

from assistant.bot.index import setup_index
from assistant.bot.search import search_docs_and_answer, get_sources
from assistant.bot.llm import get_llm, get_embedding_model


class Bot:
    def __init__(
        self,
        llm_question: Callable,
        llm_text: Callable,
        llm_code: Callable,
        llm_self_eval: Callable,
        embed: Callable,
        device: str = "cuda",
    ) -> None:
        """Constructor

        Args:
            llm_question (Callable): llm model used to determine wether the answer implies code or text generation
            llm_text (Callable): llm model used to generate a textual answer
            llm_code (Callable): llm model used to generate a code answer
            llm_self_eval (Callable): llm model used to perform self evaluation of textual answer
        """
        self.device = device
        self.llm_question = llm_question
        self.llm_text = llm_text
        self.llm_code = llm_code
        self.llm_self_eval = llm_self_eval
        self.embed = embed

        self.index_doc = setup_index("documents", self.llm_question, self.embed)

    def run(
        self,
        query: str,
    ):
        """main method to perform answer generation and sources retrieval

        Args:
            query (str): query of the user
        """

        # determine if the answer is code or text
        is_code = ask_llm_is_code(prompt=query, llm=self.llm_question)

        # if is code, call llm#3 for code generation
        if is_code:
            # TODO make a search JSON
            pre_answer = search_docs_and_answer(
                query, self.index_doc, llm=self.llm_code, embed=self.embed
            )
            sources = get_sources(pre_answer)

        # else, call llm#2 for text generation
        else:
            # run self evaluation of the answer
            pre_answer = search_docs_and_answer(
                query, self.index_doc, llm=self.llm_text, embed=self.embed
            )
            sources = get_sources(pre_answer)

            # TODO make it work
            # pre_answer = self_evaluation_loop(
            #     prompt=query, answer=pre_answer, llm=self.llm_self_eval, max_retry=5
            # )

        # run self consistency
        answer = self_consistency(
            prompt=query, pre_answer=str(pre_answer), sources=sources, is_code=is_code
        )

        return answer, sources


if __name__ == "__main__":
    MODEL_PATH = "meta-llama/Llama-2-13b-chat-hf"
    EMBEDDING_PATH = "BAAI/bge-large-en-v1.5"
    OUTPUT_PATH = os.environ["OUTPUT_PATH"]

    logger.info("Loading models")
    llm_question = get_llm(MODEL_PATH)
    llm_text = llm_question
    llm_code = llm_question
    llm_self_eval = llm_question
    logger.success("LLM model(s) loaded on device")

    embedding_model = get_embedding_model(EMBEDDING_PATH)
    logger.info("Embedding model loaded ")

    bot = Bot(
        llm_question=llm_question,
        llm_text=llm_text,
        llm_code=llm_code,
        llm_self_eval=llm_self_eval,
        embed=embedding_model,
    )

    # read the csv file containing the queries
    csv = pd.read_csv(
        os.environ["CSV_PATH"],
        sep=";",
        index_col="id",
    )

    for i in tqdm(csv.index):
        answer, sources = bot.run(query=csv.loc[i].question)
        save_output(answer, sources, OUTPUT_PATH, id=str(i))
