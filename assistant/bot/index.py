# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from typing import List
import os
import json
import asyncio

from loguru import logger
from tqdm import tqdm
from llama_index.vector_stores import (
    ElasticsearchStore,
)
from llama_index import (
    VectorStoreIndex,
    Document,
    ServiceContext,
    VectorStoreIndex,
)
from llama_index.storage.storage_context import StorageContext
from llama_index.text_splitter import TokenTextSplitter
from llama_index.llms.llm import LLM
from llama_index.embeddings import BaseEmbedding
from assistant.bot.llm import get_embedding_model


def load_documents(data_path: str) -> List[Document]:
    """Load json documents prepared through dataprep scripts

    Args:
        data_path (str): path to JSON folder

    Returns:
        List[Document]: Create custome llamaIndex documents
    """
    docs = []

    text_splitter = TokenTextSplitter(separator=" ", chunk_size=512, chunk_overlap=128)

    for file in tqdm(os.listdir(data_path)):
        data = json.load(open(os.path.join(data_path, file), "r"))
        for i, text in enumerate(text_splitter.split_text(data["text_clean"])):
            docs.append(
                Document(doc_id=data["id"] + f"_{i}", metadata=data["metas"], text=text)
            )

    return docs


def setup_index(index_name: str, llm: LLM, embed: BaseEmbedding) -> VectorStoreIndex:
    """Create the llama search index on documents. If the index already exists, jsut retrieve it.
    Otherwise perform document embedding and create the index in elasticsearch

    Args:
        index_name (str): elasticsearch index name
        llm (LLM): Embedding LLM

    Returns:
        VectorStoreIndex: llama vector index
    """
    # connect to ES
    es = ElasticsearchStore(
        index_name=index_name,
        es_url="http://0.0.0.0:9200",
    )

    # setup context
    service_context = ServiceContext.from_defaults(llm=llm, embed_model=embed)
    storage_context = StorageContext.from_defaults(vector_store=es)

    # workaround to persist data because ther is a bug in llamaIndex
    if not asyncio.get_event_loop().run_until_complete(
        es.client.indices.exists(index=index_name)
    ):
        logger.debug(f"Creating index {index_name}")
        # load documents
        documents = load_documents(
            os.path.join(os.environ["TEMP_DIR"], "data/clean_json")
        )
        logger.info(f"Loaded {len(documents)} documents")

        index = VectorStoreIndex.from_documents(
            documents,
            storage_context=storage_context,
            service_context=service_context,
            show_progress=True,
        )
    else:
        logger.debug(f"Index {index_name} already exists")
        index = VectorStoreIndex.from_vector_store(
            vector_store=es,
            service_context=service_context,
        )

    logger.info("Index setup up DONE")

    return index
