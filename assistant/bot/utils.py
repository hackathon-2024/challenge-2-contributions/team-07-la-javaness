# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from collections.abc import Callable
from typing import List
import json
import os

from assistant.bot.promtps import SYSTEM_PROMPT_ASSISTANT, SYSTEM_PROMPT_QUALITY_CHECKER


def ask_llm_is_code(prompt: str, llm: Callable):
    """method to determine wether or not the answer contains code or textual info using an llm

    Args:
        prompt (str): question of the user
        llm (Callable): llm instance to determine the nature of the answer

    Returns:
        bool: indicating if the answer is code or text
    """
    import random

    is_code = random.choice([True, False])

    return is_code


def interrogate_rag(prompt: str, is_code: bool):
    """iterrogate the rag to obtain useful sources for answer generation

    Args:
        prompt (str): question of the user
        is_code (bool): indicates wether the answer is textual or code

    Returns:
        list: list of suggested sources of the rag
    """
    # TODO : implement the rag
    # if the answer contains code, access db apis (jsons)
    if is_code:
        suggestions = ["Source 1", "Source 2", "Source 3"]
    # else, access db docs and db apis
    else:
        suggestions = ["Source 1", "Source 2", "Source 3"]
    return suggestions


def generate_pre_answer(prompt: str, sources: list, llm: Callable):
    """generate a llm answer for a question from some sources

    Args:
        prompt (str): query of the user
        sources (list): list of useful sources identified by rag
        llm (Callable): llm to perform answer generation

    Returns:
        str: answer of the llm. will be refined by self consistency
    """
    # TODO : adapt the type of the argument sources on the requires input type for llms
    # TODO : implement llm. Nature of the answer (code / text) must rely on the llm called
    answer = "Tankh you !"
    return answer


def save_output(response: List[str] | str, sources: List[str], path: str, id: str):
    """save outputs to /answers folder and /sources folder"""
    source_path = os.path.join(path, "sources")
    response_path = os.path.join(path, "answers")

    if not os.path.exists(source_path):
        os.mkdir(source_path)

    if not os.path.exists(response_path):
        os.mkdir(response_path)

    # write response
    if isinstance(response, list):  # more than one answere generated
        os.mkdir(os.path.join(response_path, str(id)))
        for i, r in enumerate(response):
            with open(os.path.join(response_path, str(id), f"{id}-{i}.txt"), "w") as f:
                f.write(r)
    else:
        with open(os.path.join(response_path, f"{id}.txt"), "w") as f:
            f.write(response)

    # write sources
    with open(os.path.join(source_path, f"{id}.txt"), "w") as f:
        f.writelines(sources)


def self_evaluation_loop(
    prompt: str, answer: str, sources: list, llm: Callable, max_retry: int = 5
):
    """Perform self evaluation on the answer of an LLM regarding a query

    Args:
        prompt (str): question of the user
        answer (str): answer of the llm
        sources (list): useful sources for answer generation
        llm (Callable): llm model instance
        max_retry (int, optional): maximal number of try to return a refined answer. Defaults to 5.

    Returns:
        str: self-evaluated refined answer
    """

    thread_checker = [
        {"role": "system", "content": SYSTEM_PROMPT_QUALITY_CHECKER},
        {"role": "user", "content": prompt},
        {"role": "assistant", "content": answer},
    ]

    first_eval = json.loads(llm(messages=thread_checker).choices[0].message.content)

    if first_eval["answer_validity"] == True:
        return answer
    else:
        thread = [
            {"role": "system", "content": SYSTEM_PROMPT_ASSISTANT},
            {"role": "user", "content": prompt},
        ]

        validity = False
        retry = 0
        checker_prompt = first_eval["additional_prompt"]

        while not validity and retry < max_retry:
            retry += 1
            thread.append({"role": "user", "content": checker_prompt})
            new_answer = llm(messages=thread).choices[0].message.content
            thread_checker[2] = {"role": "assistant", "content": new_answer}
            thread.append({"role": "assistant", "content": new_answer})
            eval = json.loads(llm(messages=thread_checker).choices[0].message.content)
            
            validity, checker_prompt = (
                eval["answer_validity"],
                eval["additional_prompt"],
            )
        return new_answer


def self_consistency(prompt: str, pre_answer: str, sources: list, is_code: bool):
    """perform self consistency on the answer of a llm regarding the prompt

    Args:
        prompt (str): query of the user
        pre_answer (str): answer provided by the llm behind the self consistency module
        sources (list): useful sources for answer generation
        is_code (bool): indicates if the answer must be code or text

    Returns:
        str: refined answer of the module
    """

    # TODO implement

    return pre_answer
