# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from typing import List
import os
import re

from loguru import logger

from llama_index.vector_stores import (
    MetadataFilters,
    FilterOperator,
)

from llama_index import (
    VectorStoreIndex,
    get_response_synthesizer,
    Response,
    ServiceContext,
)

from llama_index.llms.llm import LLM
from llama_index.embeddings import BaseEmbedding
from llama_index.retrievers import VectorIndexRetriever
from llama_index.query_engine import RetrieverQueryEngine
from llama_index.postprocessor import SimilarityPostprocessor
from llama_index.response_synthesizers.type import ResponseMode
from llama_index.llms import ChatMessage, MessageRole
from llama_index.prompts import ChatPromptTemplate


COMPANIES = [
    "amazon",
    "apple",
    "bing",
    "booking",
    "crowdtangle",
    "facebook",
    "google",
    "linkedin",
    "pinterest",
    "snapchat",
    "tiktok",
    "wikipedia",
    "twitter",
    "youtube",
]

REGEX_COMPANY = f"({'|'.join(COMPANIES)})"


def search_docs_and_answer(
    query: str, index: VectorStoreIndex, llm: LLM, embed: BaseEmbedding
) -> Response:
    """Perform search and generate the response

    Args:
        query (str): query
        index (VectorStoreIndex): search index
        llm (LLM): LLM for query embedding

    Returns:
        Response: llamaIndex response
    """
    company = extract_company(query=query)

    if company:
        logger.info(f"company found: {company}")

        retriever = VectorIndexRetriever(
            index=index,
            similarity_top_k=10,
            filters=MetadataFilters.from_dicts(
                [{"key": "company", "value": company, "operator": FilterOperator.EQ}]
            ),
        )
        
    else:
        retriever = VectorIndexRetriever(
            index=index,
            similarity_top_k=10,
        )

    # configure response synthesizer
    response_synthesizer = get_response_synthesizer(
        service_context=ServiceContext.from_defaults(llm=llm, embed_model=embed),
        response_mode=ResponseMode.COMPACT,
    )

    # assemble query engine
    query_engine = RetrieverQueryEngine.from_args(
        retriever=retriever,
        response_synthesizer=response_synthesizer,
        node_postprocessors=[SimilarityPostprocessor(similarity_cutoff=0.7)],
    )

    # query
    logger.info(f"Query: {query}")
    response = query_engine.query(query)
    logger.info(f"Response: {response}")

    return response


def get_sources(response: List[Response]) -> List[str]:
    """extract url sources"""
    sources = set()

    for node in response.source_nodes:
        if node.metadata.get("url"):
            sources.add(node.metadata.get("url"))

    sources = list(sources)
    logger.info(f"Sources: {sources}")
    return sources


def extract_company(query: str) -> str | None:
    """Extract company name from folder"""

    match = re.search(REGEX_COMPANY, query.lower())

    if not match:
        return None

    return match.group(0)
