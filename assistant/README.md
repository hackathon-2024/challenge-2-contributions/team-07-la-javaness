[![LinkedIn](https://img.shields.io/badge/LinkedIn-Profile-blue)](https://www.linkedin.com/company/la-javaness/mycompany/)

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://www.lajavaness.com/">
    <img src="logo_LJN_extend.png" alt="Logo" width="880" height="280">
  </a>

  <h3 align="center">Hackathon 2024 : Digital Services (h)Acked</h3>

  <p align="center">
Solution proposed by the La Javaness team in response to the challenge of using state-of-the-art techniques in automated language processing to develop open-source artificial intelligence to facilitate the elaboration of projects, particularly research projects, based on platform data.
    <br />
    <a href="https://www.peren.gouv.fr/actualites/2023-12-21_hackathon2024"><strong>Explore the Challenge Web Page»</strong></a>
    <br />
    <br />
    <a href="https://hackathon.peren.fr/hackathon2024_challenge2_v2501-EN.6267f634.pdf">Read the Challenge Rules</a>
    ·
    <a href="https://www.lajavaness.com/">La Javaness Web Page</a>
    ·
    <a href="https://gitlab.com/ljn/teams/data/pocs/hackathon-peren-sandbox">Request Feature</a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->
<h2 id="about-our-solution">🎯 About Our Solution</h2>

<h2 id="getting-started">🏁 Getting Started</h2>

#### Data Preparation
Décrire décrire
```
bash prepare.sh -user
```

<h2 id="getting-started">🏃 Run Assistant</h2>

#### Data Preprocessing
Décrire Décrire
```
bash preprocess.sh
```
#### Run Assistant
Décrire décrire
```
bash run.sh
```

