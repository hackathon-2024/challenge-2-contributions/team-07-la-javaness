# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import pathlib
from typing import List, Dict
import re
from uuid import uuid4
import json

from loguru import logger
import bs4
from tqdm import tqdm
import pypdf

TYPE_INFO = {
    "transparency_report": [r"transparency[\s_]report", r"transparency"],
    "terms_and_policy": [r"terms", r"policy"],
    "documentation": [r"endpoint"],
    "endpoint": [r"api", r"docs?", r"documentation"],
    "cheatsheet": [r"cheatsheet"],
    "examples": [r"examples?"],
    "statistics": [r"statistics?", r"stats"],
    "homepage": [r"home( |-)?page"],
    "report": [r"reports?"],
}


def main(data_path: str, output_path: str):
    """Process all HTMLs and PDFs in datapath recursively and save
    extracted data in a json file in the output path.

    For each file a data dictionnary is created containing:
     * id = a generated uuid
     * title = html or pdf file title
     * metas = raw metadata dictionnary extracted for the original files
     * text_raw = raw text as extracted from file
     * text_clean = cleaned text without header/footer
     * url = for html the original url if found in the file meta data
     * file_type = indicate if original file is an html or a pdf
     * company = which company does it document come from ? (amazon, twitter, ...)
    """
    data_path = pathlib.Path(data_path)
    i = 0
    j = 0

    # get all HTMLs recursively
    for i, file in enumerate(tqdm(data_path.glob("**/*.html"))):
        uuid = str(uuid4())
        logger.trace(file)
        data = process_html(file)
        data["id"] = uuid
        data["metas"]["file_type"] = "html"
        data["metas"].update(get_custom_metas(file))
        json.dump(data, open(os.path.join(output_path, f"{i}_{uuid}.json"), "w"))
    logger.success("HTMLs DONE")

    # get all PDF recursively
    for j, file in enumerate(tqdm(data_path.glob("**/*.pdf"))):
        uuid = str(uuid4())
        logger.trace(file)
        data = process_pdf(file)
        data["id"] = uuid
        data["metas"]["file_type"] = "pdf"
        data["metas"].update(get_custom_metas(file))
        json.dump(data, open(os.path.join(output_path, f"{i+j}_{uuid}.json"), "w"))
    logger.success("PDFs DONE")

    # get all MD recursively
    for k, file in enumerate(tqdm(data_path.glob("**/*.md"))):
        uuid = str(uuid4())
        logger.trace(file)
        datas = process_md(file)
        
        for l, data in enumerate(datas):
            data["id"] = uuid
            data["metas"]["file_type"] = "md"
            data["metas"].update(get_custom_metas(file))
            json.dump(
                data, open(os.path.join(output_path, f"{i+j+k+l}_{uuid}.json"), "w")
            )
            
    logger.success("MDs DONE")


def process_html(file: str) -> Dict:
    soup = bs4.BeautifulSoup(open(file, "r"))
    
    data = {
        "raw_metas": _get_metas_html(soup),
        "text_raw": soup.get_text(separator="\n", strip=True),
        "text_clean": _clean_soup(soup).get_text(separator="\n", strip=True),
    }
    
    data["metas"] = {
        "title": _get_title(soup),
        "url": data["raw_metas"].get("og:url", None),
    }

    if not len(data["text_clean"]) and len(data["text_raw"]):
        logger.debug(f"{file}: cleaned text is empty")

    if not data["metas"]["url"]:
        logger.debug(f"{file}: no url found")

    return data


def process_pdf(file: str) -> Dict:
    reader = pypdf.PdfReader(file)
    text = "\n\n".join([page.extract_text() for page in reader.pages])
    
    data = {
        "raw_metas": dict(reader.metadata),
        "text_raw": text,
        "text_clean": text,
    }
    
    data["metas"] = {
        "title": " : ".join(file.as_posix().split("/")[-2:])[:-4],
        "url": None,
    }

    return data


def process_md(file: str) -> List[Dict]:
    text = open(file, "r").read()

    datas = []

    for sub_t in text.split("Resource URL:"):
        logger.trace(sub_t.split()[0])
        
        datas.append(
            {
                "metas": {
                    "url": sub_t.split()[0],
                    "title": " : ".join(file.as_posix().split("/")[-2:])[:2],
                },
                "text_raw": sub_t,
                "text_clean": " ".join(sub_t.split()[1:]),
                "raw_metas": {},
            }
        )
    return datas


def get_custom_metas(file: pathlib.PosixPath) -> Dict:
    return {"company": _get_company(file), "info_type": _get_info_type(file)}


def _get_metas_html(s: bs4.BeautifulSoup) -> Dict:
    """Extract metas from the file"""
    metas = {}

    for m in s("meta"):
        if m.has_attr("property"):
            metas[m["property"]] = m.get("content")
        
        elif m.has_attr("name"):
            metas[m["name"]] = m.get("content")

    return metas


def _get_title(s: bs4.BeautifulSoup) -> str:
    """get title from <title> html tag"""
    return s.find("title").get_text() if s.find("title") else None


def _clean_soup(soup):
    # get main soup content elements
    content_elts = soup.body
    if not content_elts:
        content_elts = soup

    # 1. try to find content by div id
    containers = content_elts.findAll(True, {"id": ["bodyContent", "content"]})
    if containers:
        # sort to make sure we take the outtest one in case of in
        containers = sorted(containers, key=len)
        return containers[-1]

    # 2. try to find content by div role (main)
    containers = content_elts.findAll(True, {"role": ["main"]})
    if containers:
        # sort to make sure we take the outtest one in case of in
        containers = sorted(containers, key=len)
        return containers[-1]

    # 3. try to find content by div class
    containers = content_elts.findAll(
        True,
        {
            "class": [
                "main-content",
                "page-content",
                "wiki-content",
                "content-wrap",
                "a4fcb3bfb4",  # cas bizarre de booking.com
            ]
        },
    )
    if containers:
        # sort to make sure we take the outtest one in case of in
        containers = sorted(containers, key=len)
        return containers[-1]

    # 4. try to find content by tag
    containers = content_elts.findAll("article")
    if containers:
        # sort to make sure we take the outtest one in case of in
        containers = sorted(containers, key=len)
        return containers[-1]

    # If nothing was found, then remove elements that are purely navigation or placeholder information
    elements = []
    elements.extend(
        content_elts(
            [
                "footer",
                "head",
                "header",
                "nav",
                "script",
                "form",
                "button",
                "input",
                "noscript",
                "iframe",
                "urlset",
            ]
        )
    )
    elements.extend(
        content_elts(
            True,
            {
                "class": [
                    "btn",
                    "button",
                    "navbar",
                    "cookies",
                    "notification",
                    "breadcrumb",
                    "skip-link",
                    "comment",
                ]
            },
        )
    )
    elements.extend(content_elts(True, {"role": ["dialog", "alert", "navigation"]}))
    for elt in elements:
        elt.decompose()

    return content_elts


def _get_company(file_path: pathlib.PosixPath) -> str:
    """Extract company name from folder"""
    folder = file_path.as_posix().split("/")[-2]
    company = folder.split("_")[0].lower()

    # special case for twitter
    if company == "x":
        return "twitter"
    
    elif company == "booking.com":
        return "booking"
    
    else:
        return company


def _get_info_type(file_path: pathlib.PosixPath) -> list[str]:
    types = []

    for type_, regexes in TYPE_INFO.items():
        if any([re.search(regex, file_path.stem.lower()) for regex in regexes]):
            types.append(type_)

    if not types:
        logger.warning(f"No info type found for {file_path}")

    return types


if __name__ == "__main__":
    DATA_PATH = os.path.join(os.environ["TEMP_DIR"])
    OUTPUT_PATH = os.path.join(DATA_PATH, "clean_json")

    main(DATA_PATH, OUTPUT_PATH)
