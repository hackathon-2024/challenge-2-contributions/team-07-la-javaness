# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from assistant.dataprep.parse_files import main as prepare_dataset
import os

if __name__ == "__main__":
    DATA_PATH = os.environ["TEMP_DIR"]
    OUTPUT_PATH = os.path.join(DATA_PATH, "data/clean_json")

    if not os.path.exists(OUTPUT_PATH):
        os.mkdir(OUTPUT_PATH)

    prepare_dataset(DATA_PATH, OUTPUT_PATH)
