# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import List, Optional, Union

from pydantic import BaseModel, Field, RootModel


class QuotaItem(BaseModel):
    threshold: Optional[int] = None
    object_unit: Optional[str] = None
    time_unit: Optional[str] = None
    additional_information: Optional[str] = None


class OtherInformation(BaseModel):
    name: Optional[str] = Field(None, description="Name of the piece of information")
    content: Optional[str] = Field(None, description="Piece of information")


class ParameterDescriptionItem(BaseModel):
    name: str = Field(..., description="Field name")
    type: Optional[str] = Field(None, description="Field type")
    description: Optional[str] = Field(None, description="Field description")
    is_required: Optional[bool] = Field(
        None, description="Is the parameter required (soes not always apply)?"
    )
    example: Optional[str] = Field(None, description="")


class ParameterDescription(RootModel):
    root: List[ParameterDescriptionItem] = Field(..., description="")


class DataStructure(BaseModel):
    name: Optional[str] = None
    content: Optional[ParameterDescription] = None


class OnlinePlatformApiDocumentationV10(BaseModel):
    platform_name: str = Field(..., description="Platform's name")
    description: Optional[str] = Field(None, description="Endpoint's description")
    endpoint_name: str = Field(..., description="Platform's endpoint name")
    endpoint_url: str = Field(..., description="Platform's endpoint URL")
    target_audience: str = Field(
        ..., description="Who can use the API? Public? Research?"
    )
    http_method: str = Field(..., description="GET or POST (or other)")
    quota: List[QuotaItem] = Field(
        ...,
        description="Are there any limitations to using this endpoint? (e.g. number of queries per minute,...)",
    )
    response_data: ParameterDescription
    query_parameters: Optional[ParameterDescription] = None
    data_structures: Optional[List[DataStructure]] = None
    example_query: str = Field(..., description="")
    example_response: str = Field(..., description="")
    other_information: Optional[Union[List, OtherInformation]] = Field(
        None, description="Other information that may be available on the web page."
    )


if __name__ == "__main__":
    print(
        OnlinePlatformApiDocumentationV10.parse_file(
            "/home/jeanbaptiste/Documents/Python/hackathon-peren-sandbox/jb/dataprep/example.json",
        )
    )
