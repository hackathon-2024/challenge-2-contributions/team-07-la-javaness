# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import pandas as pd
import os
from loguru import logger

PROMPT_SYSTEM_W_SOURCE = """You are an expert of {platform}'s API, based on the following sources you answer questions straight to the point : {source}"""


def prepare_json_data(data_dir):
    query_file_path = os.path.join(data_dir, "input", "questions.csv")
    answer_dir = os.path.join(data_dir, "output", "answers")
    sources_dir = os.path.join(data_dir, "output", "sources")

    # Read queries
    input_query = pd.read_csv(query_file_path, sep=";", index_col="id")

    # Output data
    output_data = []
    for id in input_query.index:
        # Read answers
        if os.path.exists(os.path.join(answer_dir, f"{id:03d}")):
            ans_dir = os.path.join(answer_dir, f"{id:03d}")
            answers = []
            for file in os.listdir(ans_dir):
                with open(os.path.join(ans_dir, file), "r") as f:
                    answers.append(f.read().rstrip("\n"))
        elif os.path.exists(os.path.join(answer_dir, f"{id:03d}.txt")):
            with open(os.path.join(answer_dir, f"{id:03d}.txt"), "r") as f:
                answers = [f.read().rstrip("\n")]
        else:
            answers = []

        # Read sources
        if os.path.exists(os.path.join(sources_dir, f"{id:03d}.txt")):
            with open(os.path.join(sources_dir, f"{id:03d}.txt"), "r") as f:
                source = [f.read()]
        else:
            source = []
        for answer in answers:
            output_data.append(
                {
                    "id": id,
                    "query": input_query.loc[id]["question"],
                    "answer": answer,
                    "source": source,
                }
            )

    return output_data


def create_msg_thread(data_sample, prompt_system):
    data_sample["messages"] = [
        {"role": "system", "content": prompt_system},
        {"role": "user", "content": data_sample["query"]},
        {"role": "assistant", "content": data_sample["answer"]},
    ]
    return data_sample


def write_json_dataset(data, output_file):
    pd.DataFrame(data).to_json(output_file, orient="records", lines=True)


if __name__ == "__main__":
    ### Create json file dataset for fine tuning via SFT ###
    ROOT_DIR = os.path.join(os.environ["TEMP_DIR"], "assistant")

    logger.info("Preparing dataset")
    data = prepare_json_data(os.path.join(ROOT_DIR, "train"))
    for line in data:
        create_msg_thread(
            line,
            prompt_system=PROMPT_SYSTEM_W_SOURCE.format(
                platform="major internet platform", source=line["source"][0]
            ),
        )
    write_json_dataset(data, "train.jsonl")
    logger.success("Dataset done")
