# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from huggingface_hub import snapshot_download

snapshot_download(
    repo_id="mistralai/Mistral-7B-v0.1",
    local_dir="mistralai/Mistral-7B-v0.1",
)
