<!--
SPDX-FileCopyrightText: 2024 2024 la_javaness
SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: EUPL-1.2
-->

# Team Name : La Javaness

## Licence / License
	
:fr: Ce code a été produit pour le Challenge 2 du Hackathon 2024 co-organisé par le PEReN et la Commission Européenne : DSA RAG Race. Il est délivré comme récupéré à la fin de l'événement et peut donc ne pas être exécutable.
Sauf mention contraire, le code source est placé sous licence publique de l'Union Européenne, version 1.2 (EUPL-1.2).
Les données des plateformes présentes sur ce projet sont la propriété des plateformes et ne sont fournies qu'à titre d'illustration pour le bon fonctionnement du projet. Elles ne seront en aucun cas mises à jour. Les données complètes peuvent être trouvées vers ce lien : https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

:gb: This code was developed for Challenge 2 of the Hackathon 2024 co-organized by the PEReN and the European Commission: DSA RAG Race. It is released as retrieved at the end of the event and may therefore not be executable.
Unless otherwise specified, the source code is licensed under the European Union Public License, version 1.2 (EUPL-1.2).
The data of platforms contained in this project are the property of the platforms and are provided for illustrative purposes only. They will not be updated under any circumstances. The complete data can be found at the following link: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

[![LinkedIn](https://img.shields.io/badge/LinkedIn-Profile-blue)](https://www.linkedin.com/company/la-javaness/mycompany/)

  <h3 align="center">Hackathon 2024 : Digital Services (h)Acked</h3>

  <p align="center">
Solution proposed by the La Javaness team in response to the challenge of using state-of-the-art techniques in automated language processing to develop open-source artificial intelligence to facilitate the elaboration of projects, particularly research projects, based on platform data.
    <br />
    <a href="https://www.peren.gouv.fr/actualites/2023-12-21_hackathon2024"><strong>Explore the Challenge Web Page»</strong></a>
    <br />
    <br />
    <a href="https://hackathon.peren.fr/hackathon2024_challenge2_v2501-EN.6267f634.pdf">Read the Challenge Rules</a>
    ·
    <a href="https://www.lajavaness.com/">La Javaness Web Page</a>
    ·
    <a href="https://gitlab.com/ljn/teams/data/pocs/hackathon-peren-sandbox">Request Feature</a>
  </p>
</div>

<!-- ABOUT THE PROJECT -->
<h2 id="about-our-solution">🎯 About Our Solution</h2>

N.B : Il faudra télécharger `elasticsearch-8.12.0` sur le [site](https://www.elastic.co/fr/downloads/past-releases/elasticsearch-8-12-0) et le mettre en racine du projet pour exécuter le code.

Our solution mainly relies on a Vector database using Elastic Search where the documents are embedded and stored. The retrieval is done using Llama Index implemented retrieval augmented generation pipeline with an open source embedding model, currently the [`Salesforce/SFR-Embedding-Mistral`](https://huggingface.co/Salesforce/SFR-Embedding-Mistral) and a Large Language Model, here Llama-2. 

It also includes : 
- a self evaluation loop responsible for checking and correcting answers through self quality checking and re-generation.
- a self consistency module responsible for computing consistency of generated answers and choosing the best answer based on several metrics (Levensthein Distance, Bleu/Rouge metrics)
- a script for fine-tuning the LLM using LLoRA method in a supervised fashion to improve generated answer for API related questions.

<h2 id="getting-started">🏁 Getting Started</h2>

#### Data Preparation
```bash
bash prepare.sh -user
```

It loads module `python/3.11.5` and simply install the requirements directly from `requirements.txt`.
The main packages used are : `transformers`, `datasets`, `llama-index`, `torch`

<h2 id="getting-started">🏃 Run Assistant</h2>

#### Data Preprocessing
```bash
bash preprocess.sh
```
This script is responsible for parsing Markdown and HTML files. It prepares the data for the embedding step. At this stage, the script also extract some useful metadata to improve performance during the retrieval step. Especially, platform's name as well as the type of document are extracted.

The main packages used are : `BeautifulSoup4`, `pypdf`

#### Run Assistant

```bash
bash run.sh
```

The main packages used are : `llama_index`, `pypdf`, `transformers`, `torch`, `peft`
