# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

echo "Parse documents"

export TEMP_DIR=$PWD
#module load python/3.11.5
python -m assistant.dataprep.main


