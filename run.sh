#!/bin/bash

# SPDX-FileCopyrightText: 2024 la_javaness
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

export CSV_PATH=$1
export OUTPUT_PATH=$2
export TEMP_DIR=$PWD
export ES_HEAP_SIZE=2g

mkdir $PWD/es-data


echo "start up - elasticsearch"
elasticsearch-8.12.0/bin/elasticsearch & 
/bin/sleep 30 # change to num secs needed for ES to start correctly

echo "Execute python script"
#module load python/3.11.5
python -m assistant.bot.main
